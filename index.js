const puppeteer = require("puppeteer");
const fs = require("fs-extra");
const data = require("./data.json");
const hbs = require("handlebars");
const path = require("path");
const moment = require("moment");
const axios = require("axios");

const compile = async function(templateName, data) {
  const filePath = path.join(process.cwd(), "templates", `${templateName}.hbs`);
  const html = await fs.readFile(filePath, "utf-8");
  return hbs.compile(html)(data);
};

hbs.registerHelper("dateFormat", function(value, format) {
  return moment(value).format(format);
});

(async function() {
  try {
    console.log("starting...");

    const browser = await puppeteer.launch();
    const page = await browser.newPage();

    // get list of top cats (top 12)
    data.cats = await getCats(12);
    const content = await compile("cats", data);

    await page.setContent(content);
    await page.emulateMedia("print");

    await page.goto(`data:text/html,${content}`, {
      waitUntil: "networkidle0"
    });

    await page.pdf({
      path: `${Date.now()}.pdf`,
      format: "A4",
      printBackground: true,
      displayHeaderFooter: true,
      headerTemplate: "<h1>HEADER</h1>",
      footerTemplate: "<h1>FOOTER</h1>",
      margin: {
        top: "1in",
        right: "1in",
        bottom: "1in",
        left: "1in"
      },
      preferCSSPageSize: true
    });

    console.log("done");
    await browser.close();
    process.exit();
  } catch (e) {
    console.log("error: ", e);
    process.exit(0);
  }
})();

/* RESOURCES 

Documentation

    Main page:
        https://github.com/GoogleChrome/puppeteer/blob/v1.10.0/docs/api.md#pagepdfoptions        

    Waiting for assets to load:
        https://github.com/GoogleChrome/puppeteer/issues/728        

    Using as google cloud function:
        https://cloud.google.com/blog/products/gcp/introducing-headless-chrome-support-in-cloud-functions-and-app-engine    

*/

async function getCats(num) {
  try {
    const response = await axios.get(
      "https://api.thecatapi.com/v1/images/search",
      {
        headers: { "x-api-key": "b4104155-b5a9-4cff-b476-e15c6c56fc5a" },
        params: {
          order: "desc",
          limit: num,
          format: "json",
          size: "full"
        }
      }
    );
    return response.data;
  } catch (e) {
    console.log(error);
    throw e;
  }
}
